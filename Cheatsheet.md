

# Synth
```
    :prophet
    :dsaw
    :fm
    :tb303
    :pulse
```

# Envelope
play 38, attack: 0.4, attack_level: 1.5, decay: 0.5, decay_level:0.4, sustain: 0.5, sustain_level: 1, release: 0.1

# Samples
```
    :ambi_
    :bass_
    :elec_
    :perc_
    :guit_
    :drum_
    :misc_
    :bd_
```